<?php include_once "head.php" ?>
<?php include_once "menu.php" ?>
</header>
<main class="categories">
    <div class="container">
        <ul class="menu__dropdown">
            <li class="menu__category">
                <a class="menu__cLink" href="products.php">
                    Okrycia wierzchnie
                </a>
            </li>
            <li class="menu__category">
                <a class="menu__cLink" href="products.php">
                    Sukienki
                </a>
            </li>
            <li class="menu__category">
                <a class="menu__cLink" href="products.php">
                    Bluzki
                </a>
            </li>
            <li class="menu__category">
                <a class="menu__cLink" href="products.php">
                    Koszule
                </a>
            </li>
            <li class="menu__category">
                <a class="menu__cLink" href="products.php">
                    Swetry
                </a>
            </li>
            <li class="menu__category">
                <a class="menu__cLink" href="products.php">
                    Spódnice
                </a>
            </li>
            <li class="menu__category">
                <a class="menu__cLink" href="products.php">
                    Spodnie
                </a>
            </li>
            <li class="menu__category">
                <a class="menu__cLink" href="products.php">
                    Bluzy
                </a>
            </li>
            <li class="menu__category">
                <a class="menu__cLink" href="products.php">
                    Torebki
                </a>
            </li>
            <li class="menu__category">
                <a class="menu__cLink" href="products.php">
                    Obuwie
                </a>
            </li>
            <li class="menu__category">
                <a class="menu__cLink" href="products.php">
                    Akcesoria
                </a>
            </li>
            <li class="menu__category">
                <a class="menu__cLink" href="products.php">
                    Kombinezony
                </a>
            </li>
            <li class="menu__category">
                <a class="menu__cLink" href="products.php">
                    Komplety
                </a>
            </li>
        </ul>
    </div>
</main>
<?php include_once "footer.php" ?>
</body>
</html>
