<?php include_once "head.php" ?>
<?php include_once "menu.php" ?>
</header>
<main class="item">
    <section class="section section--1">
        <div class="container">
            <div class="section__content">
                <?php include_once "sidebar.php" ?>
                <div class="section__preview preview">
                    <div class="preview__main">
                        <img class="preview__img" src="img/produkt/1.jpg">
                    </div>
                    <ul class="preview__list">
                        <li class="preview__item preview__item--active">
                            <img class="preview__img" src="img/produkt/1.jpg">
                        </li>
                        <li class="preview__item">
                            <img class="preview__img" src="img/produkt/2.jpg">
                        </li>
                        <li class="preview__item">
                            <img class="preview__img" src="img/produkt/3.jpg">
                        </li>
                        <li class="preview__item">
                            <img class="preview__img" src="img/produkt/4.jpg">
                        </li>
                    </ul>
                </div>
                <div class="section__details details">
                    <h4 class="details__name">BLUZKA Z KORONKĄ</h4>
                    <p class="details__price">199, 99 PLN</p>
                    <p class="details__color">Kolor</p>
                    <ul class="details__colorList">
                        <li class="details__colorItem details__colorItem--1  details__colorItem--active"></li>
                        <li class="details__colorItem details__colorItem--2"></li>
                        <li class="details__colorItem details__colorItem--3"></li>
                        <li class="details__colorItem details__colorItem--4"></li>
                    </ul>
                    <label for="rozmiar">Wybierz rozmiar:</label>
                    <select name="rozmiar" id="rozmiar" class="details__size">
                        <option value="0">XS</option>
                        <option value="1">S</option>
                        <option value="2">M</option>
                        <option value="3">L</option>
                    </select>
                    <a class="details__btn btn btnMain" href="#">
                        <span>kup</span>
                    </a>
                    <p class="details__describe">
                        Opis produktu:
                    </p>
                    <p class="details__text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    </p>
                </div>
            </div>
        </div>
    </section>
</main>
<?php include_once "footer.php" ?>
</body>
</html>