<?php get_header(); ?>


<main class="categories">

    <div class="container">
        <?php echo do_shortcode( '[product_categories]' ); ?>
    </div>
</main>

<?php get_footer(); ?>