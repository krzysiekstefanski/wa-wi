<?php

function load_stylesheets() {

    wp_register_style('stylesheet', get_template_directory_uri() . '/style.css', '', 1, 'all');
    wp_enqueue_style('stylesheet');

}

add_action('wp_enqueue_scripts', 'load_stylesheets');

function load_javascript() {

    wp_register_script('custom', get_template_directory_uri() . '/app.js', 'jquery', 1, true);
    wp_enqueue_script('custom');

}

add_action('wp_enqueue_scripts', 'load_javascript');

// Menu //
add_theme_support('menus');

// Rejestracja nowego menu //
register_nav_menus(
  array(
      'top-menu' => 'Górne Menu',
  )
);

add_filter ( 'nav_menu_css_class', 'so_37823371_menu_item_class', 10, 4 );

function so_37823371_menu_item_class ( $classes, $item, $args, $depth ){
    $classes[] = 'menu__item';
    return $classes;
}

add_filter( 'nav_menu_link_attributes', function($atts) {
    $atts['class'] = "menu__btn btnMain btnMain--reverse";
    return $atts;
}, 100, 1 );



add_theme_support('post-thumbnail');

add_image_size('post_image', 1100, 750, true);

function mytheme_add_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );


function new_submenu_class($menu) {
    $menu = preg_replace('/ class="sub-menu"/','/ class="menu__dropdown" /',$menu);
    return $menu;
}

add_filter('wp_nav_menu','new_submenu_class');


function woocommerce_product_category( $args = array() ) {

    $parentid = get_queried_object_id();

    $args = array(
        'parent' => $parentid
    );

    $terms = get_terms( 'product_cat', $args );

    if ( $terms ) {

        echo '<ul class="menu__dropdown">';

        foreach ( $terms as $term ) {

            echo '<li class="menu__category">';

//            woocommerce_subcategory_thumbnail( $term );


            echo '<a href="' .  esc_url( get_term_link( $term ) ) . '" class="menu__cLink' . $term->slug . '">';
            echo $term->name;
            echo '</a>';

            echo '</li>';


        }

        echo '</ul>';

    }
}

add_action( 'woocommerce_before_shop_loop', 'woocommerce_product_category', 100 );

function so_38878702_remove_hook(){
    remove_action( 'woocommerce_before_shop_loop', 'woocommerce_output_all_notices', 10 );
    remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
    remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
}
add_action( 'woocommerce_before_shop_loop', 'so_38878702_remove_hook', 1 );