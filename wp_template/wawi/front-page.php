<?php get_header(); ?>
<div class="hero">
    <div class="hero__bg">
        <div class="container">
            <div class="hero__content">
                <div class="hero__headerWrapper">
                    <h3 class="hero__subheader">
                        Jesień-zima 2019
                    </h3>
                    <h2 class="hero__header">
                        Wyraź swój styl w nowym sezonie
                    </h2>
                    <a class="hero__button btn btnMain" href="#">
                        <span>Zobacz</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<main class="home">
    <section class="section section--1">
        <div class="section__banners">
            <a class="banner banner--1" href="products.php">
                <img class="banner__img" src="<?= get_template_directory_uri() ?> /img/bannery/1.jpg" alt="Nowości w sklepie">
                <span class="banner__header">new</span>
                <p class="banner__text">Nowości w sklepie</p>
                <span class="banner__link">- sprawdź</span>
            </a>
            <a class="banner banner--2"  href="products.php">
                <img class="banner__img" src="<?= get_template_directory_uri() ?> /img/bannery/2.jpg" alt="Promocje i wyprzedaże">
                <span class="banner__header">sale</span>
                <p class="banner__text">Promocje i wyprzedaże</p>
                <span class="banner__link">- skorzystaj</span>
            </a>
            <a class="banner banner--3"  href="products.php">
                <img class="banner__img" src="<?= get_template_directory_uri() ?> /img/bannery/3.jpg" alt="Bestsellery">
                <span class="banner__header">best</span>
                <p class="banner__text">Bestsellery</p>
                <span class="banner__link">- zobacz</span>
            </a>
        </div>
    </section>
    <section class="section section--2">
        <div class="container">
            <div class="section__header">
                <h3 class="section__headerTitle">
                    Najmodniejsze w tym sezonie
                </h3>
                <p class="section__headerText">
                    Zainspiruj się najmodniejszymi trendami w tym sezonie i znajdź ubrania, które wyrażą Twój wyjątkowy styl!
                </p>
            </div>
            <div class="section__topProducts">
            <?php
            $params = array('posts_per_page' => 5, 'post_type' => 'product');
            $wc_query = new WP_Query($params);
            ?>
            <?php if ($wc_query->have_posts()) : ?>
                <?php while ($wc_query->have_posts()) :
                    $wc_query->the_post(); ?>
                <div class="section__product product">
                    <div class="product__content">
                        <img class="product__img" src="<?php the_post_thumbnail_url('post-image'); ?>">
                        <a  class="product__link btn btnMain btnMain--transparent"  href="product_single.php" data-action="see"><span>Zobacz</span></a>
                        <a  class="product__link btn btnMain btnMain--transparent"  href="#" data-action="buy"><span>kup</span></a>
                    </div>
                    <h4 class="product__name"><?php the_title(); ?></h4>
                    <?php $price = get_post_meta( get_the_ID(), '_price', true ); ?>
                    <p class="product__price"><?php echo wc_price( $price ); ?></p>
                </div>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else:  ?>
                <p>
                    <?php _e( 'No Products'); ?>
                </p>
            <?php endif; ?>
            </div>
            <div class="section__clickForMore">
                <a href="products.php" class="btn btnMain"><span>Więcej</span></a>
            </div>
        </div>
    </section>
    <section class="section section--3">
        <div class="container">
            <div class="section__banners">
                <a class="banner banner--instagram" href="https://www.instagram.com/wawi_boutique/" target="_blank">
                    <i class="banner__icon">
                        <?= file_get_contents(get_template_directory_uri() . "/img/instagram.svg"); ?>
                    </i>
                    <p class="banner__text">Znajdź nas na instagramie</p>
                </a>
                <a class="banner banner--facebook" href="https://www.facebook.com/wawiboutique/" target="_blank">
                    <i class="banner__icon">
                        <?= file_get_contents(get_template_directory_uri() . "/img/fb.svg"); ?>
                    </i>
                    <p class="banner__text">Znajdź nas na facebooku</p>
                </a>
            </div>
        </div>
    </section>
</main>
<?php get_footer(); ?>
