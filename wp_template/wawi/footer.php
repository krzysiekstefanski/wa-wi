<footer class="section__footer footer">
    <div class="container">
        <div class="footer__content">
            <a href="index.php">
                <h2 class="footer__brand">
                    Wa-Wi Boutique
                </h2>
            </a>
            <p class="footer__text">
                Moda to nasza pasja, dlatego postanowiliśmy zająć się nią zawodowo. Prowadzimy internetowy sklep z modną odzieżą damską, oraz stacjonarny butik w Gryfinie przy ul. Marii Konopnickiej 14a.
            </p>
            <ul class="footer__contact">
                <li class="footer__item">
                    <a class="footer__link" href="https://www.facebook.com/wawiboutique/" target="_blank">
                        <i class="footer__icon">

                        </i>
                    </a>
                </li>
                <li class="footer__item">
                    <a class="footer__link" href="https://www.instagram.com/wawi_boutique/" target="_blank">
                        <i class="footer__icon">

                        </i>
                    </a>
                </li>
                <li class="footer__item">
                    <a class="footer__link" href="mailto:sklep@wa-wi.pl">
                        sklep@wa-wi.pl
                    </a>
                </li>
                <li class="footer__item">
                    <a class="footer__link" href="tel:48534368826">
                        + 48 534 368 826
                    </a>
                </li>
            </ul>

            <ul class="footer__menu footer__menu--1">
                <li class="footer__item">
                    <a class="footer__link" href="#">
                        O nas
                    </a>
                </li>
                <li class="footer__item">
                    <a class="footer__link" href="#">
                        Regulamin sklepu
                    </a>
                </li>
                <li class="footer__item">
                    <a class="footer__link" href="#">
                        Polityka prywatności
                    </a>
                </li>
                <li class="footer__item">
                    <a class="footer__link" href="#">
                        Reklamacje i zwroty
                    </a>
                </li>
            </ul>

            <ul class="footer__menu footer__menu--2">
                <li class="footer__item">
                    <a class="footer__link" href="#">
                        Zaloguj
                    </a>
                </li>
                <li class="footer__item">
                    <a class="footer__link" href="#">
                        Promocje
                    </a>
                </li>
                <li class="footer__item">
                    <a class="footer__link" href="#">
                        Nowości
                    </a>
                </li>
                <li class="footer__item">
                    <a class="footer__link" href="#">
                        Bestsellery
                    </a>
                </li>
            </ul>
        </div>
    </div>
</footer>
<script src="https://kit.fontawesome.com/0411afede9.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js"></script>
<?php wp_footer(); ?>
</body>
</html>