<div class="section__sidebar sidebar">
    <ul>
        <li class="sidebar__category">
            <a class="sidebar__mLink" href="products.php">
                Kategorie
            </a>
        </li>
        <ul class="sidebar__dropdown">
            <li class="sidebar__category">
                <a class="sidebar__cLink" href="products.php">
                    Okrycia wierzchnie
                </a>
            </li>
            <li class="sidebar__category">
                <a class="sidebar__cLink" href="products.php">
                    Sukienki
                </a>
            </li>
            <li class="sidebar__category">
                <a class="sidebar__cLink" href="products.php">
                    Bluzki
                </a>
            </li>
            <li class="sidebar__category">
                <a class="sidebar__cLink" href="products.php">
                    Koszule
                </a>
            </li>
            <li class="sidebar__category">
                <a class="sidebar__cLink" href="products.php">
                    Swetry
                </a>
            </li>
            <li class="sidebar__category">
                <a class="sidebar__cLink" href="products.php">
                    Spódnice
                </a>
            </li>
            <li class="sidebar__category">
                <a class="sidebar__cLink" href="products.php">
                    Spodnie
                </a>
            </li>
            <li class="sidebar__category">
                <a class="sidebar__cLink" href="products.php">
                    Bluzy
                </a>
            </li>
            <li class="sidebar__category">
                <a class="sidebar__cLink" href="products.php">
                    Torebki
                </a>
            </li>
            <li class="sidebar__category">
                <a class="sidebar__cLink" href="products.php">
                    Obuwie
                </a>
            </li>
            <li class="sidebar__category">
                <a class="sidebar__cLink" href="products.php">
                    Akcesoria
                </a>
            </li>
            <li class="sidebar__category">
                <a class="sidebar__cLink" href="products.php">
                    Kombinezony
                </a>
            </li>
            <li class="sidebar__category">
                <a class="sidebar__cLink" href="products.php">
                    Komplety
                </a>
            </li>
        </ul>
        <li class="sidebar__category">
            <a class="sidebar__mLink" href="products.php">
                Nowości
            </a>
        </li>
        <li class="sidebar__category">
            <a class="sidebar__mLink" href="products.php">
                Wyprzedaż
            </a>
        </li>
        <li class="sidebar__category">
            <a class="sidebar__mLink" href="products.php">
                Bestsellery
            </a>
        </li>
    </ul>
</div>