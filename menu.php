<nav class="menu__nav">
    <div class="container h-100">
        <div class="menu__content">

            <div class="menu__brand">
                <a href="index.html">
                    <h1 class="menu__logo">
                        Wa-Wi Boutique
                    </h1>
                </a>
            </div>

            <ul class="menu__icons">
                <li class="menu__item">
                    <i class="menu__icon">
                        <a class="menu__link" href="#">
                            <?= file_get_contents("img/bag.svg"); ?>
                        </a>
                    </i>
                </li>
                <li class="menu__item">
                    <i class="menu__icon">
                        <a class="menu__link" href="#">
                            <?= file_get_contents("img/user.svg"); ?>
                        </a>
                    </i>
                </li>
            </ul>

            <div class="menu__hamburger">
                <i>
                    <a href="#">
                        <?= file_get_contents("img/hamburger.svg"); ?>
                    </a>
                </i>
            </div>

            <ul class="menu__list menu__list--hamburger">
                <li class="menu__item menu__item--index">
                    <a class="menu__btn btnMain btnMain--reverse" href="index.php">
                        <span>strona główna</span>
                    </a>
                </li>
                <li class="menu__item menu__item--dropdown">
                    <a class="menu__btn btnMain btnMain--reverse" href="categories.php">
                        <span>kategorie</span>
                    </a>
                    <ul class="menu__dropdown">
                        <li class="menu__category">
                            <a class="menu__cLink" href="products.html">
                                Okrycia wierzchnie
                            </a>
                        </li>
                        <li class="menu__category">
                            <a class="menu__cLink" href="products.html">
                                Sukienki
                            </a>
                        </li>
                        <li class="menu__category">
                            <a class="menu__cLink" href="products.html">
                                Bluzki
                            </a>
                        </li>
                        <li class="menu__category">
                            <a class="menu__cLink" href="products.html">
                                Koszule
                            </a>
                        </li>
                        <li class="menu__category">
                            <a class="menu__cLink" href="products.html">
                                Swetry
                            </a>
                        </li>
                        <li class="menu__category">
                            <a class="menu__cLink" href="products.html">
                                Spódnice
                            </a>
                        </li>
                        <li class="menu__category">
                            <a class="menu__cLink" href="products.html">
                                Spodnie
                            </a>
                        </li>
                        <li class="menu__category">
                            <a class="menu__cLink" href="products.html">
                                Bluzy
                            </a>
                        </li>
                        <li class="menu__category">
                            <a class="menu__cLink" href="products.html">
                                Torebki
                            </a>
                        </li>
                        <li class="menu__category">
                            <a class="menu__cLink" href="products.html">
                                Obuwie
                            </a>
                        </li>
                        <li class="menu__category">
                            <a class="menu__cLink" href="products.html">
                                Akcesoria
                            </a>
                        </li>
                        <li class="menu__category">
                            <a class="menu__cLink" href="products.html">
                                Kombinezony
                            </a>
                        </li>
                        <li class="menu__category">
                            <a class="menu__cLink" href="products.html">
                                Komplety
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="menu__item">
                    <a class="menu__btn btnMain btnMain--reverse" href="#">
                        <span>nowości</span>
                    </a>
                </li>
                <li class="menu__item">
                    <a class="menu__btn btnMain btnMain--reverse" href="#">
                        <span>wyprzedaż</span>
                    </a>
                </li>
                <li class="menu__item">
                    <a class="menu__btn btnMain btnMain--reverse" href="#">
                        <span>bestsellery</span>
                    </a>
                </li>
            </ul>

            <div class="menu__search search">
                <input type="text" class="search__input">
            </div>

        </div>
    </div>
</nav>