<?php include_once "head.php" ?>
<?php include_once "menu.php" ?>
</header>
<main class="categories categories--dress">
    <section class="section section--1">
        <div class="container">
            <div class="section__content">
                <?php include_once "sidebar.php" ?>
                <div class="section__products">

                    <div class="section__product product">
                        <div class="product__content">
                            <img class="product__img" src="img/produkty/1.jpg">
                            <a  class="product__link btn btnMain btnMain--transparent"  href="product_single.php" data-action="see"><span>Zobacz</span></a>
                            <a  class="product__link btn btnMain btnMain--transparent"  href="#" data-action="buy"><span>kup</span></a>
                        </div>
                        <h4 class="product__name">BLUZKA Z KORONKĄ</h4>
                        <p class="product__price">199, 99 PLN</p>
                    </div>

                    <div class="section__product product">
                        <div class="product__content">
                            <img class="product__img" src="img/produkty/2.jpg">
                            <a  class="product__link btn btnMain btnMain--transparent"  href="product_single.php" data-action="see"><span>Zobacz</span></a>
                            <a  class="product__link btn btnMain btnMain--transparent"  href="#" data-action="buy"><span>kup</span></a>
                        </div>
                        <h4 class="product__name">BLUZKA Z KORONKĄ</h4>
                        <p class="product__price">199, 99 PLN</p>
                    </div>

                    <div class="section__product product">
                        <div class="product__content">
                            <img class="product__img" src="img/produkty/3.jpg">
                            <a  class="product__link btn btnMain btnMain--transparent"  href="product_single.php" data-action="see"><span>Zobacz</span></a>
                            <a  class="product__link btn btnMain btnMain--transparent"  href="#" data-action="buy"><span>kup</span></a>
                        </div>
                        <h4 class="product__name">BLUZKA Z KORONKĄ</h4>
                        <p class="product__price">199, 99 PLN</p>
                    </div>

                    <div class="section__product product">
                        <div class="product__content">
                            <img class="product__img" src="img/produkty/4.jpg">
                            <a  class="product__link btn btnMain btnMain--transparent"  href="product_single.php" data-action="see"><span>Zobacz</span></a>
                            <a  class="product__link btn btnMain btnMain--transparent"  href="#" data-action="buy"><span>kup</span></a>
                        </div>
                        <h4 class="product__name">BLUZKA Z KORONKĄ</h4>
                        <p class="product__price">199, 99 PLN</p>
                    </div>

                    <div class="section__product product">
                        <div class="product__content">
                            <img class="product__img" src="img/produkty/5.jpg">
                            <a  class="product__link btn btnMain btnMain--transparent"  href="product_single.php" data-action="see"><span>Zobacz</span></a>
                            <a  class="product__link btn btnMain btnMain--transparent"  href="#" data-action="buy"><span>kup</span></a>
                        </div>
                        <h4 class="product__name">BLUZKA Z KORONKĄ</h4>
                        <p class="product__price">199, 99 PLN</p>
                    </div>

                    <div class="section__product product">
                        <div class="product__content">
                            <img class="product__img" src="img/produkty/6.jpg">
                            <a  class="product__link btn btnMain btnMain--transparent"  href="product_single.php" data-action="see"><span>Zobacz</span></a>
                            <a  class="product__link btn btnMain btnMain--transparent"  href="#" data-action="buy"><span>kup</span></a>
                        </div>
                        <h4 class="product__name">BLUZKA Z KORONKĄ</h4>
                        <p class="product__price">199, 99 PLN</p>
                    </div>

                    <div class="section__product product">
                        <div class="product__content">
                            <img class="product__img" src="img/produkty/7.jpg">
                            <a  class="product__link btn btnMain btnMain--transparent"  href="product_single.php" data-action="see"><span>Zobacz</span></a>
                            <a  class="product__link btn btnMain btnMain--transparent"  href="#" data-action="buy"><span>kup</span></a>
                        </div>
                        <h4 class="product__name">BLUZKA Z KORONKĄ</h4>
                        <p class="product__price">199, 99 PLN</p>
                    </div>

                    <div class="section__product product">
                        <div class="product__content">
                            <img class="product__img" src="img/produkty/8.jpg">
                            <a  class="product__link btn btnMain btnMain--transparent"  href="product_single.php" data-action="see"><span>Zobacz</span></a>
                            <a  class="product__link btn btnMain btnMain--transparent"  href="#" data-action="buy"><span>kup</span></a>
                        </div>
                        <h4 class="product__name">BLUZKA Z KORONKĄ</h4>
                        <p class="product__price">199, 99 PLN</p>
                    </div>

                </div>
            </div>
        </div>
    </section>
</main>
<?php include_once "footer.php" ?>
</body>
</html>