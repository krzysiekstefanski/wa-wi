$(function() {

    $('.preview__item').on('click', showImagePreview);

    $('.details__size').select2({
        width: '100%',
        minimumResultsForSearch: Infinity,
    });

    $('.preview__main img').on('swipeleft', function() {
        console.log('swipe left');
        $('.preview__item--active').removeClass('preview__item--active').next().addClass('preview__item--active');
    });

    $('.preview__item').on('click', function() {
        $('.preview__item--active').removeClass('preview__item--active');
        $(this).addClass('preview__item--active');
    });

    $('.details__colorItem').on('click', function() {
        $('.details__colorItem--active').removeClass('details__colorItem--active');
        $(this).addClass('details__colorItem--active');
    });

    $('.product__link[data-action="see"]').css('transition:', 'top .3s ease, opacity .3s ease');
    $('.product__link[data-action="buy"]').css('transition:', 'bottom .3s ease, opacity .3s ease');
    $('.menu__dropdown').css('transition:', 'transform .3s ease');

    $('.product').on('click', function() {
        $('.product--active').removeClass('product--active');
        $(this).addClass('product--active');
        $(this).on('mouseleave', function() {
            $(this).removeClass('product--active');
            $(this).off('mouseleave');
        })
    });

    $('.menu__hamburger').on('click', function() {
        $('.menu__list').toggleClass('menu__list--active');
        $('.menu__dropdown--active').removeClass('menu__dropdown--active');
    });

    // $('.menu__item--dropdown').on('mouseover', function() {
    //     $(this).find('.menu__dropdown').addClass('menu__dropdown--hover');
    //     //$('.menu__list').toggleClass('menu__list--active');
    // });
    //
    // $('.menu__item--dropdown').on('mouseleave', function() {
    //     $(this).find('.menu__dropdown').removeClass('menu__dropdown--hover');
    //     //$('.menu__list').toggleClass('menu__list--active');
    // });

    // $('.menu__dropdown').on('mouseleave', function() {
    //     $(this).removeClass('menu__dropdown--hover');
    // });

    $('.menu__item--dropdown').on('touchstart', function() {
        var url = $(this).find('.menu__btn').attr('href');
        window.location.href = url;
        //$('.menu__list').toggleClass('menu__list--active');
    });

    // $('.banner').on('click', function() {
    //     var url = $(this).find('.banner__link').attr('href');
    //     window.location.href = url;
    //     //$('.menu__list').toggleClass('menu__list--active');
    // });
});

function showImagePreview() {
    var smImage = $(this).find('img');
    var mainImage = $('.preview__main').find('img');
    mainImage.attr('src', smImage.attr('src'));
}